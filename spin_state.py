"""
A collection of classes of spin objects that interact with polarization.


Classes:
    SpinState: Spin state objects.
    Waveplate: Waveplate objects that performs a rotation.
    Polarizer: Polarizer object that performs a projection.
    SLM: Spatial Light Modulator (SLM) object that applies OAM.

"""

from cmath import pi
import numpy as np
import numpy.typing as npt
from scipy import linalg
from typing import Literal


class SpinState(object):
    """Spin state objects.

    Attributes:
        spin: 2-dimensional complex vector of a spin state.
        intensity: Total intensity/probability of spin state.
    """

    def __init__(
        self,
        spin_state: npt.NDArray[np.float64 | np.complex128],
        basis: Literal["RL", "HV", "DA"] = "RL",
    ) -> None:
        """Creates a spin state object.

        Args:
            spin_state: Ket or vector notation of spin state.
            basis: Basis of input state. `RL` (default), `HV`, or `DA`.
        """
        if basis == "RL":
            self.spin = spin_state

        # TODO: Untested
        elif basis == "HV":
            H = spin_state[0]
            V = spin_state[1]
            self.spin = np.array([H - V, H + V]) / np.sqrt(2)

        # TODO: Untested
        elif basis == "DA":
            D = spin_state[0]
            A = spin_state[1]
            self.spin = np.array([D - 1j * A, D + 1j * A]) / np.sqrt(2)

        else:
            raise ValueError("Error: Not a valid vector basis.")

    def __repr__(self) -> str:
        return "Spin State: {}\nalpha: {}, beta: {}\nIntensity: {}\n".format(
            np.round(self.spin, 2),
            np.round(self.alpha, 2),
            np.round(self.beta, 2),
            np.round(self.intensity, 2),
        )

    @property
    def spin(self) -> npt.NDArray[np.complex128]:
        """
        Spin state superposition coefficients as psi = [alpha, beta].

        This should be normalized such that `psi**2 = 1`.
        """
        return self._spin

    @spin.setter
    def spin(self, spin_state: npt.ArrayLike) -> None:
        spin_state = np.array(spin_state)
        self._alpha = spin_state[0]
        self._beta = spin_state[1]
        self._spin = np.copy(spin_state)
        self._intensity = np.inner(self.spin, self.spin.conjugate()).real

    @property
    def alpha(self) -> complex:
        """Complex coefficient of `l = +1` spin."""
        return self._alpha

    @property
    def beta(self) -> complex:
        """Complex coefficient of `l = -1` spin."""
        return self._beta

    @property
    def intensity(self) -> float:
        """Total intensity of current spin state.

        Equal to one for pure states."""
        return self._intensity


class SpinStateFromAngle(SpinState):
    def __init__(self, theta: float, phi: float) -> None:
        """
        Creates a spin state object from angles on the bloch sphere.

        Args:
            theta: Polar angle on the bloch sphere (in rads).
            phi: Azimuthal angle on the bloch sphere (in rads).
        """

        self.set_spin(theta, phi)

    def __repr__(self) -> str:
        return "{}theta: {}, phi: {}\n".format(
            super().__repr__(), np.round(self.theta, 2), np.round(self.phi, 2)
        )

    @property
    def theta(self) -> float:
        """Polar angle (from the z-axis) on the bloch sphere (in rads)."""
        return self._theta

    @property
    def phi(self) -> float:
        """Azimuthal angle (from the z-axis) on the bloch sphere (in rads)."""
        return self._phi

    def set_spin(self, theta: float, phi: float) -> None:
        """
        Converts angles on bloch sphere to spin states.

        Args:
            theta: Polar angle on the bloch sphere (in rads).
            phi: Azimuthal angle on the bloch sphere (in rads).
        """
        self._theta = theta
        self._phi = phi
        alpha = np.cos(theta / 2)
        beta = np.exp(1j * phi) * np.sin(theta / 2)
        self.spin = (alpha, beta)


class Waveplate(object):
    """Waveplate object.

    Attributes:
        operator: Rotation operator a waveplate applies onto a spin state.
    """

    _sigma_x = np.array([[0, 1], [1, 0]])
    _sigma_y = np.array([[0, -1j], [1j, 0]])  # type: ignore
    _sigma_z = np.array([[1, 0], [0, -1]])

    def __init__(self, alpha: float, beta: float) -> None:
        """Creates a waveplate object.

        Args:
            alpha: Birefringence axis angle on bloch sphere (in rads).
            beta: Retardance - rotation on bloch sphere (in rads).
                Quarter Waveplate = ``pi/2``.
                Half Waveplate = ``pi``.
        """
        self.alpha = alpha
        self.beta = beta
        direction = (
            np.cos(alpha) * Waveplate._sigma_x
            + np.sin(alpha) * Waveplate._sigma_y
        )
        self._operator = linalg.expm((-1j * beta / 2) * direction)

    def __repr__(self) -> str:
        return "Waveplate Operator {}\nbeta: {}, alpha: {}\n".format(
            np.round(self.operator, 2),
            np.round(self.beta, 2),
            np.round(self.alpha, 2),
        )

    @property
    def operator(self) -> npt.NDArray[np.complex_]:
        """
        Waveplate operator.

        This is effectively a rotation operator about some arbitrary
        axis in the linear plane on the bloch sphere.
        """
        return self._operator


class Polarizer(SpinStateFromAngle):
    """Polarizer object.

    Attributes:
        operator: Projection operator a polarizer applies onto a spin state.
    """

    def __init__(self, theta: float, phi: float) -> None:
        """Creates a polarizer object.

        Args:
            theta: Polar angle on the bloch sphere (in rads).
            phi: Azimuthal angle on the bloch sphere (in rads).

        """
        super().__init__(theta, phi)
        self._operator = np.outer(self.spin, self.spin.conjugate())

    def __repr__(self) -> str:
        return "{}Operator:\n{}".format(super().__repr__(), self.operator)

    @property
    def operator(self) -> npt.NDArray[np.complex_]:
        return self._operator


class SLM:
    """Spatial Light Modulator (SLM) object that applies OAM.

    Attributes:
        operator: Applies an OAM onto a spin state.
    """

    def __init__(self, l: int, phi: float) -> None:
        """
        Creates a Spatial Light Modulator (SLM) object.

        The SLM has the capability of creating orbital angular
        momentum (OAM), but only writes a phase on `|H>`.

        Args:
            l: Rotational mode number or OAM state number.
                Also known as topological charge.
            phi: Azimuthal angle on beam wavefront.
        """
        self.l = l
        self.phi = phi
        gamma = l * phi / 2
        self._operator = np.array(
            [
                [-1j * np.sin(gamma), np.cos(gamma)],
                [np.cos(gamma), -1j * np.sin(gamma)],
            ]
        )

    @property
    def operator(self) -> npt.NDArray[np.complex_]:
        return self._operator
