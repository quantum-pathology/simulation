# Simulation


* [Overview](#overview)
* [Requirements](#requirements)
* [Installation](#installation)
* [Usage](#usage)
    * [Setup](#setup)
    * [Double Waveplate Test](#double-waveplate-test)
    * [OAM](#oam-simulation)
* [Useful Math](#useful-math)
    * [Spin State](#spin-state)
    * [SLM & OAM](#slm--oam)
* [Contact](#contact)

## Overview

Simulation of laser beam spin states in the experimental setup. 


## Requirements

- A working version of `git`. This can be installed from [here](https://git-scm.com/download/). 
- `Python 3.10+`
    - Non-conda version is recommended, installed from [python.org](https://www.python.org/downloads/). 


## Installation

Clone the Repository. Note that you must have credentials to access to this 
repository:

    git clone https://git.uwaterloo.ca/medical-tissue-imaging/medical-tissue-imaging-simulation.git

Create a virtual Python environment:

    cd medical-tissue-imaging-simulation
    python -m venv .venv

Activate the virtual environment. This is the command to use whenever you want 
to invoke this specific virtual environment in the terminal:

    .venv/Scripts/activate

You should now see `(.venv)` preceding the working directory in the terminal. 

Install required packages:

    pip install -r requirements.txt

Congratulations! You have completed the installation. Now you can open your 
favourite editor, and start running experiments! Make sure that when
selecting a kernel to run the notebooks, you pick the `.venv` kernel.

### Troubleshooting Installation

**Note**: if you get an execution policy error when activating the virtual 
environment, run in an elevated privileges Powershell (as admin):

    Set-ExecutionPolicy RemoteSigned

## Usage

The only file you should be interacting with is: 
[birefringence_simulation.ipynb](birefringence_simulation.ipynb). 
The module [spin_state.py](spin_state.py) contains all the important spin 
objects, including polarizers, waveplates and the SLM. The module 
[unit_test.py](unit_test.py) contains unit tests (as its name suggests) for 
[spin_state.py](spin_state.py)

### Setup

Run these cells whenever you restart the kernel to perform required imports and
run unit tests. 

### Double Waveplate Test

Simulating the use of two QWP to emulate a single HWP. Setup consists of fixed 
polarizer and analyzer, and two rotating QWP. 

This may or may not be useful depending on your use case. 

### Fork Grating

Simulation code to generate a fork grating. This is implemented in the `slm-imaging`.

### OAM Simulation

Contains various OAM simulations, with sliders in interactive plots. Keep in
mind that increasing the resolution in the experiments (using `xmin`, `xmax`, 
`ymin`, `ymax`) can dramatically reduce the performance and responsiveness of 
the plots. To change the parameters of the simulation, you can modify the states
of various components of the setup at the beginning of each definition of the 
function `oam_simulation`.


## Useful Math

This section contains helpful descriptions to the math in this simulation.

### Spin State

Typically, in a qubit system, $\ket{0} = \begin{pmatrix} 1 \\ 0 \end{pmatrix}$
and $\ket{1} = \begin{pmatrix} 0 \\ 1 \end{pmatrix}$. However, the spin of a
photon is represented by $\ket{1}$ and $\ket{-1}$. Note that $\theta$ is the 
polar angle and $\phi$ is the azimuthal angle on the bloch sphere, **not** 
real space.

$$
\ket{\psi} = \begin{pmatrix} \cos{\frac{\theta}{2}} \\ e^{i\phi}\sin{\frac{\theta}{2}} \end{pmatrix}
= \begin{pmatrix} \alpha \\ \beta \end{pmatrix} = \alpha\ket{1} + \beta\ket{-1}  
\mid\alpha,\beta\in\mathbb{C}
$$

$$
\ket{L} = \begin{pmatrix} \cos{0} \\ e^{i0}\sin{0} \end{pmatrix}
= \begin{pmatrix} 1 \\ 0 \end{pmatrix} = \ket{1}
$$

$$
\ket{R} = \begin{pmatrix} \cos{\frac{\pi}{2}} \\ e^{i0}\sin{\frac{\pi}{2}} \end{pmatrix}
= \begin{pmatrix} 0 \\ 1 \end{pmatrix} = \ket{-1}
$$

$$
\ket{H} = \begin{pmatrix} \cos{\frac{\pi/2}{2}} \\ e^{i0}\sin{\frac{\pi/2}{2}} \end{pmatrix}
= \frac{1}{\sqrt{2}}\ket{1} + \frac{1}{\sqrt{2}}\ket{-1}
$$

$$
\ket{V} = \begin{pmatrix} \cos{\frac{\pi/2}{2}} \\ e^{i\frac{\pi}{2}}\sin{\frac{\pi/2}{2}} \end{pmatrix}
= \frac{1}{\sqrt{2}}\ket{1} - \frac{1}{\sqrt{2}}\ket{-1}
$$

$$
\ket{D} = \begin{pmatrix} \cos{\frac{\pi/2}{2}} \\ e^{i\frac{\pi}{4}}\sin{\frac{\pi/2}{2}} \end{pmatrix}
= \frac{1}{\sqrt{2}}\ket{1} + \frac{1}{\sqrt{2}}i\ket{-1}
$$

$$
\ket{A} = \begin{pmatrix} \cos{\frac{\pi/2}{2}} \\ e^{i\frac{3\pi}{4}}\sin{\frac{\pi/2}{2}} \end{pmatrix}
= \frac{1}{\sqrt{2}}\ket{1} - \frac{1}{\sqrt{2}}i\ket{-1}
$$

### SLM & OAM

This is the general operator for a SLM. First, we change from the $\ket{RL}$ 
basis to the $\ket{HV}$ basis. This simplifies calculations involving the SLM 
as it applies a phase exclusively to $\ket{H}$. Finally, we reverse the change 
of basis back into $\ket{RL}$ and simply the expression. 


$$
\begin{align*}
    &&\ket{\psi_0} 
    &= \alpha\ket{R} + \beta\ket{L} & \\

    &\ket{R}\mapsto\ket{H} - \ket{V} & \\
    &\ket{L}\mapsto\ket{H} + \ket{V} & \\

    &&\ket{\psi_0}
    &= (\beta + \alpha)\ket{H} + (\beta - \alpha)\ket{L} \\
    
    &\ket{H}\mapsto e^{-i\ell\phi} & \\
    
    &&\ket{\psi_1}
    &= e^{-i\ell\phi}(\beta + \alpha)\ket{H} + (\beta - \alpha)\ket{L} \\

    &\ket{H}\mapsto\ket{L} + \ket{R} & \\
    &\ket{V}\mapsto\ket{L} - \ket{R} & \\

    &&&= \left(e^{-i\ell\phi}(\beta + \alpha) - (\beta - \alpha)\right)\ket{R} 
    + \left(e^{-i\ell\phi}(\beta + \alpha) + (\beta - \alpha)\right)\ket{L}  \\
    
    &&&= e^{-i\ell\frac{\phi}{2}}\left(e^{-i\ell\frac{\phi}{2}}(\beta + \alpha) 
    - e^{i\ell\frac{\phi}{2}}(\beta - \alpha)\right)\ket{R} \\
    &&&\qquad + e^{-i\ell\frac{\phi}{2}}\left(e^{-i\ell\frac{\phi}{2}}(\beta + \alpha) 
    + e^{i\ell\frac{\phi}{2}}(\beta - \alpha)\right)\ket{L} \\
    &\text{Let}\ \gamma = \ell\phi & \\

    &&&= e^{-i\frac{\gamma}{2}}\left(e^{-i\frac{\gamma}{2}}(\beta + \alpha) 
    - e^{i\frac{\gamma}{2}}(\beta - \alpha)\right)\ket{R} \\
    &&&\qquad + e^{-i\frac{\gamma}{2}}\left(e^{-i\frac{\gamma}{2}}(\beta + \alpha) 
    + e^{i\frac{\gamma}{2}}(\beta - \alpha)\right)\ket{L} \\
    
    &&&= 2e^{-i\frac{\gamma}{2}}\left[\left(\alpha\cos{\frac{\gamma}{2}} 
    - i\beta\sin{\frac{\gamma}{2}}\right)\ket{R} 
    + \left(\beta\cos{\frac{\gamma}{2}} 
    - i\alpha\sin{\frac{\gamma}{2}}\right)\ket{L}\right] \\
    &\text{Let}\ \ket{\psi_1} = 2e^{-i\frac{\gamma}{2}}\ket{\psi_2} \\
    
    &&\ket{\psi_2} 
    &= \left(\alpha\cos{\frac{\gamma}{2}} - i\beta\sin{\frac{\gamma}{2}}\right)\ket{R}
    + \left(\beta\cos{\frac{\gamma}{2}} - i\alpha\sin{\frac{\gamma}{2}}\right)\ket{L} \\
\end{align*}
$$

$$
    \therefore\hat{U}_{SLM} = 
    \begin{pmatrix}
        \cos{\frac{\gamma}{2}} & -i\sin{\frac{\gamma}{2}} \\ 
        -i\sin{\frac{\gamma}{2}} & \cos{\frac{\gamma}{2}} \\
    \end{pmatrix}
    = \hat{U}_{rot, x}
$$

$$
    \therefore\hat{U}_{SLM} = 
    \begin{pmatrix}
        \cos{\frac{\ell\phi}{2}} & -i\sin{\frac{\ell\phi}{2}} \\ 
        -i\sin{\frac{\ell\phi}{2}} & \cos{\frac{\ell\phi}{2}} \\
    \end{pmatrix}
$$
