"""Unit tests for :module:`spin_state.py`."""

from spin_state import *
import numpy as np


# Tests to verify program is not broken.
def test(
    theta: float,
    alpha: float,
    beta: float,
    expected_value: float,
    cross: bool = False,
) -> None:
    """
    Tests the simulation against standard known values.


    Input laser is ``::math:: \\ket{R}``. The setup consists of a
    polarizer, waveplate and analyzer.

    Args:
        theta: Polarizer angle on bloch sphere (in rads).
        alpha: Birefringence axis angle on bloch sphere (in rads).
        beta: Retardance (in rads).
            Quarter Waveplate = ``pi/2``.
            Half Waveplate = ``pi``.
        expected_value: Expected final normalized intensity with
            respect to the input intensity.
        cross: If ``True``, sets analyzer to be anti-aligned with analyzer.

    """
    laser_input = SpinStateFromAngle(pi / 2, 0)

    polarizer = Polarizer(pi / 2, theta)
    variable_waveplate = Waveplate(alpha, beta)
    if cross:
        analyzer_angle = theta + pi
    else:
        analyzer_angle = theta
    analyzer = Polarizer(pi / 2, analyzer_angle)

    psi_1 = SpinState(polarizer.operator @ laser_input.spin)
    psi_2 = SpinState(variable_waveplate.operator @ psi_1.spin)
    psi_3 = SpinState(analyzer.operator @ psi_2.spin)
    final_state = SpinState(psi_3.spin)

    intensity = final_state.intensity
    error_message = (
        f"Expected {expected_value} but got {np.round(intensity, 2)}."
    )

    # print(psi_1, psi_2)

    assert np.isclose(intensity, expected_value), error_message
